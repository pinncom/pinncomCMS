@extends("index")
@section("content")
    <h1>{{ $h1 }}</h1>

    <ul>
    <?foreach ($pages as $page): ?>
    <li><a href="/{{$sefName}}/{{$page->sefName}}.html"><?=$page->h1 ?></a></li>
    <? endforeach; ?>
    </ul>
    <?=$pages->render()?>
    <br/>
    {!! $body or ""  !!}
@stop
