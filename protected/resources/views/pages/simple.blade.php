@extends("index")

@section("content")
<h1>{{ $h1 or $title  }}</h1>
    <div>
        {!! $body or "<b>Пустая запись</b>" !!}
    </div>
@stop