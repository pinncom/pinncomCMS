<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("category_id")->unsigned();
            $table->foreign("category_id")->references('id')->on('categories');
            $table->string("sefName")->unique();
            $table->string("title")->nullable();
            $table->string("h1")->nullable();
            $table->string("description")->nullable();
            $table->string("keywords")->nullable();
            $table->text("body");
            $table->boolean("draft");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
