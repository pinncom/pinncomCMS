<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'SimplePageController@index');
Route::get('/{sef}.htm','SimplePageController@bySef');
Route::get('/{category}.html','PageController@index');
Route::get('/{category}/{page}.html','PageController@getPage');

