<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use Config;
use App\Category;

class PageController extends Controller
{

	public function getPage($category,$page){
		/**
		 * id категории по ее ЧПУ-имени
		 * @var \App\Category $data
		 */
		$categoryId = Category::where('sefName','=',$category)->firstOrFail()->id;

		/**
		 * Получаем страницу, если она  находится в указанной категории, не черновик и ее ЧПУ имя совпадает с указанным
		 * @var \App\Page $data
		 */
		$data = Page::where('category_id','=',$categoryId)
			         ->where('draft','=',0)
			         ->where('sefName','=',$page)
			         ->firstOrFail();

		return view('.pages.simple',$data);
	}

	/**
	 * Возвращает список записей в указанной категории
	 * @param string $category
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index($category){

		/**
		 * Категория по ее ЧПУ-имени
		 * @var \App\Category $data
		 */
		$data = Category::where('sefName','=',$category)->firstOrFail();

		/**
		 * Страницы по категории
		 * @var \App\Page $pages
		 */
		$pages = Page::where('category_id','=',$data->id)->where('draft','=',0)->simplePaginate(Config::get('app.recordsPerPage'));
		   $out=[
			   'sefName' => $data->sefName,
			   'h1'=>$data->name,
			   'title'=>$data->name,
			   'description'=>$data->description,
			   'keywords'=>$data->keywords,
			   'body'=>$data->categoryText,
			   'pages'=>$pages
		   ];
		return view('.pages.category',$out);
	   }
}
