<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SimplePage;
use App\Http\Controllers\Controller;

class SimplePageController extends Controller
{
    /**
     * Возвращает главную страницу - элемент SimplePage с Sef-адресом /
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->bySef("/");
    }

    /**
     * Возвращает простую страницу по ее ЧПУ
     * @param string $sef - ЧПУ
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bySef($sef){
        /**
         * @var \App\SimplePage - Данные о страницы из базы данных
         */
        $data = SimplePage::where('sefName','=',$sef)->firstOrFail();
        // Отрисовываем шаблон
        return view(".pages.simple",$data);
    }
}
