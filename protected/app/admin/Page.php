<?php

use SleepingOwl\Admin\Models\ModelItem;

Admin::model(\App\Page::class)->title('Записи')->with()->filters(function ()
{

})->columns(function ()
{
	Column::string('sefName', 'ЧПУ-имя');
	Column::string('title', 'Заголовок окна');
	Column::string('h1', 'Заголовок статьи');
	Column::string('body', 'Текст');
	Column::string('description', 'Мета-описание');
	Column::string('keywords', 'Мета-слова');
	Column::string('category.name', 'Категория');
	Column::string('draft', 'Черновик');
})->form(function ()
{
	FormItem::select('category_id', 'Категория')->list(\App\Category::class);
	FormItem::text('sefName', 'ЧПУ-имя');
	FormItem::text('title', 'Заголовок окна');
	FormItem::text('h1', 'Заголовок статьи(H1)');
	FormItem::ckeditor('body', 'Тело');
	FormItem::text('description', 'Мета-описание');
	FormItem::text('keywords', 'Мета-слова');
	FormItem::checkbox('draft', 'Черновик');

});