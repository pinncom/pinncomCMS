<?php

Admin::model(\App\SimplePage::class)->title('Страницы без категорий')->with()->filters(function ()
{

})->columns(function ()
{
	Column::string('sefName', 'ЧПУ-имя');
	Column::string('h1', 'Заголовок статьи');
	Column::string('title', 'Заголовок браузера');
	Column::string('description', 'Мета-описание');
	Column::string('keywords', 'Мета-слова');
	Column::string('body', 'Текст');
})->form(function ()
{
	FormItem::text('sefName', 'ЧПУ-имя');
	FormItem::text('title', 'Заголовок браузера');
	FormItem::text('h1', 'Заголовок статьи');
	FormItem::text('description', 'Мета-описание');
	FormItem::text('keywords', 'Мета-слова');
	FormItem::ckeditor('body', 'Текст');
});