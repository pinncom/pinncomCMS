<?php

Admin::model(\App\Category::class)->title('Категории записей')->with()->filters(function ()
{

})->columns(function ()
{
	Column::string('sefName', 'ЧПУ-имя');
	Column::string('name', 'Название');
	Column::string('description', 'Мета-описание');
	Column::string('keywords', 'Мета-слова');
	Column::string('categoryText', 'Текстовое описание');
})->form(function ()
{
	FormItem::text('sefName', 'ЧПУ-имя');
	FormItem::text('name', 'Название');
	FormItem::text('description', 'Мета-описание');
	FormItem::text('keywords', 'Мета-слова');
	FormItem::ckeditor('categoryText', 'Текстовое описание');
});