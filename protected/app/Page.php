<?php

namespace App;


use SleepingOwl\Models\SleepingOwlModel;

class Page extends SleepingOwlModel
{
	protected $fillable = ['sefName','h1','title','description','keywords','body','draft','category_id'];


    public function category(){
	    return $this->belongsTo('\App\Category','id');
    }
}
