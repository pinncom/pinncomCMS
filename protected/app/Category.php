<?php

namespace App;


use SleepingOwl\Models\SleepingOwlModel;

/**
 * Класс категории статьи
 * @package App
 */
class Category extends SleepingOwlModel
{
	protected $fillable = ['sefName','name','description','keywords','categoryText'];
	/**
	 * Связь со страницами
	 * @return mixed
	 */
	public function pages(){
	    return $this->has_many('Page','categoryID');
    }

	public static function getList(){
		return static::lists('name','id')->all();
	}
}
