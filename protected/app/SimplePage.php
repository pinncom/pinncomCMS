<?php

namespace App;

use SleepingOwl\Models\SleepingOwlModel;

class SimplePage extends SleepingOwlModel
{
	protected $fillable = ['sefName','h1','title','description','keywords','body'];

}
